
const webpack = require('webpack')
const _ = require('lodash')
const config = require('./webpack.config.js')
const startStub = require('./stub.js')

startStub({ port: 8080 }, initWebpack)

function initWebpack() {
	try {
		if (!config) {
			throw 'Webpack config is not defined'
		}
		const compiler = webpack(_.assign({}, config), (err, stats) => {
			if (err) {
				throw err
			}
			console.log('[WEBPACK]: Webpack is load')
			if (process.env.NODE_ENV == 'development') {
				console.log(`[BOOTSTRAP][WEBPACK]: Use development mode`)
				compiler.watch(_.extend({}, config.watchOptions), afterBuild)
			} else {
				console.log(`[WEBPACK]: Use production mode`)
				compiler.run(afterBuild)
				}
		})
	} catch(err) {
		console.error(`[BOOTSTRAP][ERROR] ${err}`)
	}
}

function afterBuild(err, rawStats) {
    if (err) {
    	return console.error('[WEBPACK]: FATAL ERROR', err)
    }

    console.log('[WEBPACK]: Build Info\n' + rawStats.toString({
        colors: true,
        chunks: false
    }))

    let stats = rawStats.toJson()

    if (stats.errors.length > 0) {
        return console.error('[WEBPACK]:', stats.errors)
    }

    if (stats.warnings.length > 0) {
        console.warn('[WEBPACK]:', stats.warnings)
    }
}