const express = require('express')

module.exports = (config = { port: 8080 }, onStart) => {
	const app = express()
	app.use(express.static('public'))
	
	app.listen(config.port, () => {
		console.log(`Now server is listen port ${config.port}`)
		console.log(`http://127.0.0.1:${config.port}`)
		onStart && onStart()
	})
}