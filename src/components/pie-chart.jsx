import React from 'react'
import { PieChart, Pie, Legend, Cell } from 'recharts'

const COLORS = ['#4caf50', '#f44336']

export const PC = (props) => (
	<div>
		<PieChart width={600} height={300}>
	        <Pie
	        	data={props.data}
	        	cx={300}
	        	cy={150}
	        	innerRadius={70}
	        	outerRadius={90}
	        	dataKey={'value'}
	        	label
	        >{
	        	COLORS.map((color, index) => <Cell key={index} fill={color} />)
	        }
	        </Pie>
    	</PieChart>
    	<div className={"chart__title"}>{props.title}</div>
	</div>
)