import React from 'react'

export const PaginationButton = (props) => {
	const { onClick, text } = props
	return (
		<button
			type="button"
			onClick={onClick}
			className="pagination__button"
		>
			{text}
		</button>
	)
}