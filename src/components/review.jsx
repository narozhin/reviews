import React from 'react'
import classNames from 'classnames'

import { Avatar } from './avatar.jsx'
import { Content } from './content.jsx'

const TYPE_POSITIVE = 'positive'
const TYPE_NEGATIVE = 'negative'

export const Review = (props = {data: {}}) => {
	const type = props.data.positive ? TYPE_POSITIVE : TYPE_NEGATIVE
	const classess = classNames(
		"review",
		`review_type_${type}`
	)
	return (
		<div className={classess} key={props.data.id}>
			<Avatar name={props.data.name} />
			<Content {...props.data} />
		</div>
	)
}