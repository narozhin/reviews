import React from 'react'

const calculateMagicNumber = (name) => {
	const code = name.trim().charCodeAt(0)
	return (code * 11) % 360
}

export const Avatar = (props) => {

	const magicNumber = calculateMagicNumber(props.name)

	const styles = {
		backgroundColor: `hsl(${magicNumber}, 50%, 60%)`
	}

	return (
		<div className={"review__avatar-wrapper"}>
			<div style={styles} className={"review__avatar"}>
				<span className={"avatar__letter"}>
					{props.name.trim()[0]}
				</span>
			</div>
		</div>
	)
}