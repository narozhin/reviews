import React from 'react'
import classNames from 'classnames'

export const SortButton = (props) => {

	const { direction, field, active, text, onClick, index } = props

	const classess = classNames("sort-button", `sort-button_active_${active}`)

	const clickHandler = () => {
		onClick && onClick(field, direction, index)
	}

	return (
		<button className={classess} type="button" onClick={clickHandler}>
			{text}
		</button>
	)
}