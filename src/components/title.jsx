import React from 'react'
import { Link } from 'react-router-dom'

export const PageTitle = (props) => (
	<div className={'page-title'}>
		<h1 className={'page-title__title'}>{props.title}</h1>
		<Link className={'page-title__link'} to={props.link.to}>{props.link.text}</Link>
	</div>
)