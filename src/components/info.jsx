import React from 'react'
import classNames from 'classnames'

const POSITIVE = 'positive'
const NEGATIVE = 'negative'

export const Info = (props) => {
	const iconType = props.positive ? POSITIVE : NEGATIVE
	const iconClass = `review__icon_type_${iconType}`
	return (
		<div className="review__info">
			<span className={classNames("info__item", "review__icon", iconClass)}></span>
			<span className={classNames("info__item", "review__name")}>{props.name}</span>
			<span className={classNames("info__item", "review__date")}>{props.date}</span>
		</div>
	)
}