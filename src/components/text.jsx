import React from 'react'

export const Text = (props) => (
	<div className={"review__text"}>{props.text}</div>
)