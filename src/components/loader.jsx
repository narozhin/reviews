import React from 'react'

export const Loader = () => (
	<div className={"loader"}>
		<span className={"loader__spinner"}></span>
	</div>
)