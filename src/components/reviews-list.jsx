import React from 'react'

import { Review } from './review.jsx'

export const ReviewsList = (props) => (
	<div className={"reviews-wrapper"}>{
		props.reviews.map((review) => (
			Review({ data: review })
		))
	}</div>
)