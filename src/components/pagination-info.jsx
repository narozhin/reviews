import React from 'react'

export const PaginationInfo = (props) => {
	const { current, count } = props
	return (
		<span className={'pagination__pagination-info'}>
			{`Страница ${current} из ${count}`}
		</span>
	)
}