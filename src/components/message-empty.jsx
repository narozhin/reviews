import React from 'react'

export const MessageEmpty = () => (
	<div className={"message-empty"}>
		У ресторана пока нет отзывов :(
	</div>
)