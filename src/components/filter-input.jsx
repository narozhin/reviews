import React from 'react'

export class FilterInput extends React.Component {

	constructor() {
		super()
		this.state = {
			value: ''
		}
		this.delay = 500
		this.timer = void 0
	}

	raiseValue = (delay = 0) => {
		if (this.timer) {
			clearInterval(this.timer)
		}
		this.timer = setTimeout(() => {
			this.props.onChange && this.props.onChange(this.state.value)
		}, delay)
	}

	changeHandler = (e) => {
		this.setState({ value: e.target.value }, () => {
			this.raiseValue(this.delay)
		})
	}

	clickHandler = () => {
		this.setState({ value: '' }, this.raiseValue)
	}

	render() {
		return (
			<div className={"controll__filter-input-wrapper"}>
				<input
					className={"controll__filter_input"} type="text"
					value={this.state.value}
					onChange={this.changeHandler}
					placeholder="Введите запрос"
				/>
				<button
					className={"filter-input__button-clear"}
					type="button"
					onClick={this.clickHandler}
				>Очистить</button>
			</div>
		)
	}
}