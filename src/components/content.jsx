import React from 'react'

import { Info } from './info.jsx'
import { Text } from './text.jsx'

export const Content = (props) => {
	return (
		<div className={"review__review-content"}>
			<Info name={props.name} positive={props.positive} date={props.date} />
			<Text text={props.text} />
		</div>
	)
}