import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import { App } from './containers'
import { getStore } from './data'

const store = getStore()
window.__store = store

const rootElement = document.getElementById('root')

render(
	<Provider store={store} >
		<App />
	</Provider>,
	document.getElementById('root')
)
