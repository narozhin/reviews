import React from 'react'
import { connect } from 'react-redux'

import { PageTitle, TimeChart, PieChart } from '../components'
import { reduceTimeStats, reducePositiveStats } from '../chart.utils.js'

export class _Charts extends React.Component {

	constructor(props) {
		super()
		this.state = {
			time: reduceTimeStats(props.reviews),
			positive: reducePositiveStats(props.reviews)
		}
	}

	render() {
		return (
			<div>
				<PageTitle
					title={`Статистика отзывов`}
					link={{ to: '/', text: 'Список'}}
				/>
				<TimeChart data={this.state.time} title={'Рост количества отзывов со временем'} />
				<PieChart data={this.state.positive} title={'Диаграмма полож./отриц. отзывов'} />
			</div>
		)
	}
}

const mapState2Props = (state) => ({
	reviews: state.reviews.raw
})

export const Charts = connect(mapState2Props)(_Charts)