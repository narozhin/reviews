import React from 'react'
import { connect } from 'react-redux'

import { loadData  } from '../data/actions.js'
import { Pages } from './pages.jsx'
import { Loader } from '../components'

class _App extends React.Component {

	componentWillMount() {
		this.props.loadData()
	}

	render() {
		return this.props.isLoading
			? <Loader />
			: <Pages />
	}
}

const mapState2Props = (state) => ({
	isLoading: state.loaders.reviews
})

const mapDispatch2Props = (dispatch) => ({
	loadData: loadData(dispatch)
})

export const App = connect(mapState2Props, mapDispatch2Props)(_App)