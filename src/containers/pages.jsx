import React from 'react'
import { Switch, Route, HashRouter } from 'react-router-dom'

import { Reviews } from './reviews.jsx'
import { Charts } from './charts.jsx'

export const Pages = (props) => (
	<div className={'wrapper'}>
		<HashRouter>
			<Switch>
				<Route exact path='/' component={Reviews} />
				<Route path='/charts' component={Charts} />
			</Switch>
		</HashRouter>
	</div>
)