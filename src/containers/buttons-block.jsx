import React from 'react'

import { SortButton } from '../components'

export class ButtonsBlock extends React.Component {

	constructor() {
		super()
		this.state = { current: 0 }
	}

	clickHandler = (field, direction, index) => {
		const { onClick } = this.props
		this.setState({ current: index }, () => {
			onClick && onClick(field, direction)
		})
	}

	render() {
		return (
			<div className={"controll__buttons-block"}>
				{
					this.props.buttons.map((meta, index) => (
						<SortButton
							{...meta}
							key={index}
							index={index}
							active={this.state.current === index}
							onClick={this.clickHandler}
						/>
					))
				}
			</div>
		)
	}
}