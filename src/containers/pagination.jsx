import React from 'react'

import { PaginationInfo, PaginationButton } from '../components'

export class Pagination extends React.Component {
	constructor(props) {
		super()
		const { reviews, inPage } = props
		this.state = this.getMeta(reviews, inPage)
	}

	getPagesCount(reviews, inPage) {
		return reviews.length % inPage === 0
			? parseInt(reviews.length / inPage)
			: parseInt(reviews.length / inPage) + 1
	}

	getViewReviews(reviews, inPage, page) {
		const start = page * inPage
		const end = start + inPage > reviews.length - 1
			? reviews.length - 1
			: start + inPage - 1
		const result = []
		for (let i = start; i <= end; i++) {
			result.push(reviews[i])
		}
		return result
	}

	getMeta(reviews, inPage, page = 0) {
		return {
			current: page,
			count: this.getPagesCount(reviews, inPage),
			reviews: this.getViewReviews(reviews, inPage, page)
		}
	}

	componentWillReceiveProps(nextProps) {
		const { reviews, inPage } = nextProps
		this.setState(this.getMeta(reviews, inPage))
	}

	clickNextHandler = () => {
		const { count, current } = this.state
		if (current < count - 1) {
			const { reviews, inPage } = this.props
			this.setState(
				this.getMeta(reviews, inPage, current + 1)
			)
		}
	}

	clickPrevHandler = () => {
		const { count, current } = this.state
		if (current > 0) {
			const { reviews, inPage } = this.props
			this.setState(
				this.getMeta(reviews, inPage, current - 1)
			)
		}
	}

	render() {
		const { view } = this.props
		return (
			<div className={'pagination'}>
				<div className={'pagination__buttons'}>
					<PaginationButton text="Предыдущая" onClick={this.clickPrevHandler} />
					<PaginationInfo current={this.state.current + 1} count={this.state.count} />
					<PaginationButton text="Следующая" onClick={this.clickNextHandler} />
				</div>
				{view({ reviews: this.state.reviews })}
			</div>
		)
	}
}