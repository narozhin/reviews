import React from 'react'

import { ButtonsBlock } from './buttons-block.jsx'
import { FilterInput } from '../components'

const BUTTONS = [
	{ text: 'По умолчанию', field: 'timestamp', direction: -1 },
	{ text: 'Положительные', field: 'positive', direction: -1 },
	{ text: 'Отрицательные', field: 'positive', direction: 1 },
	{ text: 'Новые', field: 'timestamp', direction: -1 },
	{ text: 'Старые', field: 'timestamp', direction: 1 },
]

export class Controll extends React.Component {

	filterReviews = (value) => {
		this.props.filter(value)
	}

	sortReviews = (field, direction) => {
		this.props.sort(direction, field)
	}

	render() {
		return (
			<div className={"controll-wrapper"}>
				<FilterInput onChange={this.filterReviews} />
				<ButtonsBlock onClick={this.sortReviews} buttons={BUTTONS} />
			</div>
		)
	}
}