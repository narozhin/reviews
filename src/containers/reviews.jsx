import React from 'react'
import { connect } from 'react-redux'

import { loadData, sortReviews, filterReviews } from '../data/actions.js'
import { ReviewsList, MessageEmpty, PageTitle } from '../components'
import { Pagination } from './pagination.jsx'
import { Controll } from './controll.jsx'

const _Reviews = (props) => {
	return (
		<div>
			<PageTitle
				title={`Отзывы (${props.reviews.length})`}
				link={{ to: '/charts', text: 'Статистика'}}
			/>
			<Controll sort={props.sortReviews} filter={props.filterReviews} />
			{
				props.reviews.length
					? <Pagination inPage={20} reviews={props.reviews} view={ReviewsList} />
					: <MessageEmpty />
			}
		</div>
	)
}

const mapState2Props = (state) => ({
	reviews: state.reviews.view
})

const mapDispatch2Props = (dispatch) => ({
	sortReviews: sortReviews(dispatch),
	filterReviews: filterReviews(dispatch)
})

export const Reviews = connect(mapState2Props, mapDispatch2Props)(_Reviews)