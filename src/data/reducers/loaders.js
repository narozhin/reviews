import {
	LOADING_START,
	LOADING_SUCCESS,
	LOADING_ERROR
} from '../actions-types.js'

const initialState = { 'reviews': false }

export default function(state = initialState, action = {}) {
	switch(action.type) {
		case LOADING_START:
			return { ...state, reviews: true }
		case LOADING_ERROR:
		case LOADING_SUCCESS:
			return { ...state, reviews: false }
		default:
			return state
	}
}