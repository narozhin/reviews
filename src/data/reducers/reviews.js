import moment from 'moment'

import {
	LOADING_SUCCESS,
	LOADING_ERROR,
	SORT_REVIEWS,
	FILTER_REVIEWS
} from '../actions-types.js'

const DATE_FORMAT = 'DD.MM.YYYY HH:mm'

const initialState = {
	view: [],
	raw: []
}

const parseDate = (str = '') => moment(str, DATE_FORMAT).unix()

const prepareReview = (key, review) => ({
	text: review.text,
	name: review.name,
	positive: review.positive,
	timestamp: parseDate(review.date),
	date: review.date,
	id: key
})

const sortReviews = (_reviews = [], meta = { direction: 0, field: 'timestamp' }) => {
	const reviews = _reviews.concat()
	return reviews.sort((item1, item2) => {
		const value1 = item1[meta.field]
		const value2 = item2[meta.field]
		const result = value1 > value2 ? 1 : -1
		return result * meta.direction
	})
}

const filterReviews = (_reviews = [], subStr = '') => {
	return _reviews.filter((review) => {
		return review.text.toUpperCase().includes(subStr.toUpperCase())
	})
}

const mapReviews = (data = {}) => {
	const reviews = Object.keys(data).reduce((acc, key) => {
		const item = prepareReview(key, data[key])
		acc.push(item)
		return acc
	}, [])
	return sortReviews(reviews, { field: 'timestamp', direction: -1 })
}

export default function(state = initialState, action = {}) {
	switch (action.type) {
		case LOADING_SUCCESS:
			const reviews = mapReviews(action.payload)
			return { view: reviews, raw: reviews }
		case SORT_REVIEWS:
			return {
				...state,
				view: sortReviews(state.view, action.payload)
			}
		case FILTER_REVIEWS:
			return {
				...state,
				view: filterReviews(state.raw, action.payload)
			}
		case LOADING_ERROR:
			return initialState
		default:
			return state
	}
}