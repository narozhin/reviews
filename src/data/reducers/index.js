import { combineReducers } from 'redux' 
import loaders from './loaders.js'
import reviews from './reviews.js'

export default combineReducers({
	loaders, reviews
})