import * as types from './actions-types.js'

const getData = () => fetch('https://arh.chibbistest.ru/api/reviews')
	.then((response) => response.json())


export const loadData = (dispatch) => () => {
	dispatch({ type: types.LOADING_START })
	getData().then((payload) => {
		dispatch({ type: types.LOADING_SUCCESS, payload })
	})
	.catch((e) => dispatch({ type: types.LOADING_ERROR }))
}

export const sortReviews = (dispatch) => (direction, field) => {
	dispatch({ type: types.SORT_REVIEWS, payload: { direction, field } })
}

export const filterReviews = (dispatch) => (subStr = '') => {
	dispatch({ type: types.FILTER_REVIEWS, payload: subStr })
}