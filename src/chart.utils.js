
const getTimeMeta = (timestamp) => {
	const date = new Date()
	date.setTime(timestamp * 1000)
	const year = date.getFullYear()
	const mounth = date.getMonth() + 1
	return {
		name: `${mounth}/${year}`,
		year,
		mounth
	}
}

const needNewSlot = (prevMeta, currentMeta) => {
	if (prevMeta.year < currentMeta.year) {
		return true
	}
	if (prevMeta.mounth < currentMeta.mounth) {
		return true
	}
	return false
}

const increaseUV = (slot) => {
	const current = slot.uv + 1
	return { ...slot, uv: current }
}

export const reduceTimeStats = (_reviews) => {
	const reviews = _reviews.concat().reverse()
	if (!reviews.length) {
		return []
	}
	const result = []
	let prevMeta = getTimeMeta(reviews[0].timestamp)
	result.push({ name: prevMeta.name, uv: 1 })
	return reviews.reduce((acc, review, index) => {
		if (!index) {
			return acc
		}
		let currentMeta = getTimeMeta(review.timestamp)
		if (needNewSlot(prevMeta, currentMeta)) {
			acc.push({ name: currentMeta.name, uv: 1 })
		} else {
			acc[acc.length - 1] = increaseUV(acc[acc.length - 1])
		}
		prevMeta = currentMeta
		return acc
	}, result)
}

export const reducePositiveStats = (reviews) => {
	if (!reviews.length) {
		return []
	}
	const initial = [
		{ name: 'Позитивные', value: 0 },
		{ name: 'Негативные', value: 0 }
	]
	return reviews.reduce((acc, review) => {
		const index = review.positive ? 0 : 1
		acc[index].value = acc[index].value + 1
		return acc
	}, initial)
}